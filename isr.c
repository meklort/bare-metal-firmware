#include "aquila.h"

#include <generated/csr.h>
#include <generated/soc.h>
#include <irq.h>
#include <uart.h>

uint32_t irq_unhandled_vector = 0;
uint32_t irq_unhandled_source = 0;
uint8_t irq_unhandled_vector_valid = 0;
uint8_t irq_unhandled_source_valid = 0;

void isr(uint64_t vec);
void isr_dec(void);

#ifdef CONFIG_CPU_HAS_INTERRUPT

void isr(uint64_t vec)
{
    vec = vec & 0xFFF;

    if (vec == 0x900)
    {
        // DEC interrupt
        isr_dec();
        return;
    }

    if (vec == 0x500)
    {
        // Read interrupt source
        uint32_t xirr = xics_icp_readw(PPC_XICS_XIRR);
        uint32_t irq_source = xirr & 0x00ffffff;

        __attribute__((unused)) unsigned int irqs;

        // Handle IPI interrupts separately
        if (irq_source == 2)
        {
            // IPI interrupt
            xics_icp_writeb(PPC_XICS_MFRR, 0xff);
        }
        else if (irq_source == 0)
        {
            // Unknown source, slently ignore...
        }
        else
        {
            // External interrupt
            irqs = irq_pending() & irq_getmask();

#ifndef UART_POLLING
            if (irqs & (1 << UART_INTERRUPT))
            {
                uart_isr();
            }
#endif

            if (irqs & (1 << HOSTLPCSLAVE_INTERRUPT))
            {
                lpc_slave_isr();
            }

            if (!irqs)
            {
                irq_unhandled_source = irq_source;
                irq_unhandled_source_valid = 1;
            }
        }

        // Clear interrupt
        xics_icp_writew(PPC_XICS_XIRR, xirr);

        return;
    }

    irq_unhandled_vector = vec;
    irq_unhandled_vector_valid = 1;
    while (1)
    {
    }
}

void isr_dec(void)
{
    //  For now, just set DEC back to a large enough value to slow the flood of
    //  DEC-initiated timer interrupts
    mtdec(0x000000000ffffff);
}

#else

void isr(void){};
void isr_dec(void){};

#endif
